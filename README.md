# Présentation

Ce dépôt facilite le déploiement d'un cluster kubernetes sur Nova. Le cluster est composé par défaut d'un master et de trois workers.

Le déploiement est effectué à l'aide de Heat (orchestrateur Openstack) pour la partie Nova, et de [RKE (Rancher Kubernets Engine)](https://rancher.com/docs/rke/v0.1.x/en/) pour la partie kubernetes. 

# Dans le détail

L'orchestrateur Openstack Heat crée une stack de 4 VM :
* une VM "master" qui aura les rôles de controlplane (master), etcd et worker. Elle portera l'Ingress Controller, et aura donc une floating IP. 
* trois VM auront le rôle de worker. C'est sur ces VM que s'éxécuteront les pods déployés.

Une fois la stack déployée, Heat fournit le fichier cluster.yml nécessaire déploiement par RKE du cluster Kubernetes sur les VM. En dernier lieu, RKE fournit le fichier [kubeconfig](https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/#the-kubeconfig-environment-variable). La dernière étape consiste à déployer l'Ingress Controller (Traefik) sur la VM master.

# Prérequis

* Openstack CLI avec module heat
* RKE
* kubectl

# Provisionnement des nœuds

```bash
openstack stack create -t heat/openstack-rancher-cluster.yaml  mycluster --parameter flavor=m1.medium
```

# Récupération de la clef rsa

```bash
openstack stack output show mycluster private_key -f value |tail -n +3 > (openstack stack output show mycluster key_name -f value|tail -n +3)_rsa
```

# Génération du fichier cluster.yml

```bash
openstack stack output show mycluster cluster_file -f value |tail -n +3|envsubst> cluster.yml
```

# Provisionnement du cluster

```bash
rke up
```

# Positionner KUBECONFIG

```bash
export KUBECONFIG $(pwd)/kube_config_cluster.yml
```

# Déployer traefik

```bash
kubectl apply -R -f  k8s-infra/traefik2/
```

# La suite bientôt... 
